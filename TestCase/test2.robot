*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    SeleniumLibrary
Resource    ../Resource/step.robot



*** Test Cases ***

Admin should be able Login 
    Setup url_base
    Payload Login
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    POST On Session   session    /api/login    data={"username": "${username}", "password": "${password}"}    headers=${header}
    ${body}=    Convert To String    ${resp.content}
    
    #Should Be Equal As Strings    ${body}   ${resp.json()['token']}
    Should Be Equal As Integers    ${resp.status_code}    200
    Should Contain    ${body}    ${resp.json()['token']}
    Log To Console    ${body}
    Log To Console    ${resp.json()['token']}
Admin should be able to Get All User reqres
    Setup url_base
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    GET On Session    session    /api/users/

    Should Be Equal As Integers    ${resp.status_code}    200
    Log To Console    ${resp.json()['page']}
    Log To Console    ${resp.status_code}
    Log To Console    ${resp.json()['data'][0]['email']}



Admin should be create User
    Setup url_base
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    POST On Session   session    /api/users    data={"name": "${name}", "job": "${job}"}    headers=${header}
    ${payload}=    Convert To String    ${resp.content}
    Should Be Equal As Integers    ${resp.status_code}    201
    Should Contain    ${payload}    ${resp.json()['id']}
    Log To Console    ${resp.json()['id']}
    Log To Console    ${payload}

Admin should be able to update User 
    Setup url_base
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    PUT On Session    session    /api/users/2    data={"name": "${name}", "job": "${job}"}    headers=${header}    
    ${body_req}=    Convert To String    ${resp.content}
    Should Be Equal As Integers    ${resp.status_code}    200
    Should Contain    ${body_req}    ${resp.json()['updatedAt']}
    #Log To Console    ${body_req}

Admin should be able to delete User 
    Setup url_base
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    DELETE On Session   session    /api/users/2     headers=${header}    
    ${body_req}=    Convert To String    ${resp.content}
    Should Be Equal As Integers    ${resp.status_code}    204
    #Log To Console    ${body_req}

Admin should be able to secek single user
    Setup url_base
    Get single User    1
    Get single User    2
    Get single User    5
    Get single USer    7
   

