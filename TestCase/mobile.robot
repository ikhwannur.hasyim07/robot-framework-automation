*** Settings ***
Library    AppiumLibrary


*** Variables ***
${base_url}    http://127.0.0.1:4723/wd/hub
${platformName}    Android
${appPackage}    com.amartha.investor
${appActivity}    com.amartha.investor.MainActivity
${udid}    emulator-5554
${deviceName}    AndroidSDK
${masuk}    //android.widget.Button[@content-desc="Masuk"]
${email}    id=00000000-0000-00bf-0000-003d00000004
${password}    id=00000000-0000-00bf-0000-003d00000004
${button_login}    //android.widget.Button[@content-desc="Masuk"]



*** Test Cases ***
Open application
    Open Application   ${base_url}    platformName=${platformName}    appActivity=${appActivity}   appPackage=${appPackage}    deviceName=${deviceName}    udid=${udid}
    Wait Until Page Contains Element   ${masuk}
    Click Element    ${masuk}
    Click Element    ${button_login}
    #Click Element    xpath=//android.widget.Button[@content-desc="Masuk"]
    #Input Text    locator    ikhwannur.hasyim07@gmail.com