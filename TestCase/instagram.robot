*** Settings ***
Library    AppiumLibrary

*** Variables ***
${base_url}    http://127.0.0.1:4723/wd/hub
${platformName}    Android
${appPackage}    com.instagram.android
${appActivity}    com.instagram.mainactivity.MainActivity
${udid}    emulator-5554
${deviceName}    AndroidSDK

*** Test Cases ***
Open Application
    Open Application    ${base_url}    platformName=${platformName}    appActivity=${appActivity}   appPackage=${appPackage}    deviceName=${deviceName}    udid=${udid}