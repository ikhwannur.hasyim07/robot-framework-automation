***Settings***
Library    SeleniumLibrary
Library    RequestsLibrary
Library    Collections
   

***Variables***
${base_url}     https://reqres.in
${user}     2


***Test Cases***
Get single user 
    Create Session    session    ${base_url}
    ${resp}=    GET On Session    session    /api/users/${user} 

#cek status code
    Status Should Be    200    ${resp}

#cek body ada yg namanya janet
    ${body}=    Convert To String    ${resp.content}
    Should Contain    ${body}    Janet

#check header contenttype aplication/json 
    ${Contenttype}=    Get From Dictionary    ${resp.headers}    Content-Type
    Log To Console    ${Contenttype}
    Should Be Equal    ${Contenttype}    application/json; charset=utf-8



      
    