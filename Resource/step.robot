*** Settings ***
Library    RequestsLibrary
Library    Collections
Variables    ../Variable/dev_env.py



*** Keywords ***
Setup url_base
    Create Session    session    ${url_base}

Payload Login
    ${payload}=    Create List  eve.holt@reqres.in  cityslicka

Post session Login 
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=    POST On Session   session    /api/login    data={"username": "${username}", "password": "${password}"}    headers=${header}
Get single User
    [Arguments]    ${user_id}
    ${header}=    Create Dictionary    Content-Type=application/json
    ${resp}=       GET On Session    session    /api/users/${user_id}
    ${body}=    Convert To String    ${resp.content}
    Should Be Equal As Integers    ${resp.status_code}    200
    Log To Console    ${body}



